import 'package:flutter/material.dart';

void main() {
  var themeData = new ThemeData(
      // primaryColor: Colors.deepOrangeAccent,
      accentColor: Colors.deepOrange,
      brightness: Brightness.dark);

  return runApp(MaterialApp(
    home: Home(),
    theme: themeData,
  ));
}

//stateless
// class Home extends StatelessWidget {
//   final barColor = const Color(0xFF261ae60);
//   final bgColor = const Color(0xFFDAE0E2);
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: bgColor,
//       appBar: AppBar(
//         title: Text("Menu"),
//         backgroundColor: barColor,
//       ),
//       body: Center(
//         child: Text('Hello World'),
//       ),
//       floatingActionButton:
//           FloatingActionButton(backgroundColor: barColor, onPressed: () {}),
//     );
//   }
// }

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final barColor = const Color(0xFF261ae60);
  final bgColor = const Color(0xFFDAE0E2);

  int number = 1;

  void _onAdd() {
    setState(() {
      number = number + 1;
    });
  }

  void _onSub() {
    setState(() {
      number = number - 1;
    });
  }

  _textColor() {
    if (number > 0) {
      return Colors.green;
    } else {
      return Colors.red;
    }
  }

  Widget _bodyWedget() {
    return new Container(
      padding: const EdgeInsets.all(10.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              number.toString(),
              style: TextStyle(fontSize: 30, color: _textColor()),
            ),
            RaisedButton(
              onPressed: _onAdd,
              child: Text("Add"),
              color: Colors.green,
            ),
            RaisedButton(
              onPressed: _onSub,
              child: Text("Sub"),
              color: Colors.red,
            )
          ],
        ),
      ),
    );
  }

  double _floatingSize() {
    if (number < 1) {
      return 30.0;
    } else {
      return number.toDouble() + 30.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Menu"),
      ),
      body: _bodyWedget(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, size: _floatingSize()), onPressed: _onAdd),
    );
  }
}
